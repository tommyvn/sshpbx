## The SSH PBX

**A system to route SSH connections**

This is far from complete but it kinda works. The client SSH key is a user, the user name the client is connecting to is an endpoint.

Currently uses pickled objects for the routing. This is obviously crazy and will become a plugin system but for now, fire up a python shell and add some test data:

    :::python
    import pickle
    f = open("map_key_to_user", "wb")
    authorizedKeys = {
        "ssh-rsa {put your ssh RSA key here}" : "admin",
        "ssh-dsa {put another users ssh DSA key here}": "tom"
    }
    pickle.dump(authorizedKeys, f)
    f.close()

    f = open("map_request_to_endpoint", "wb")
    endpoints = {
        'admin': {
            'example': {
                'endpoint': (
                    'notroot', 'example.com', 22,
                    '-----BEGIN RSA PRIVATE KEY----- {RSA private key for accessing notroot@example.com} -----END RSA PRIVATE KEY-----',
                    'ssh-rsa {RSA public key for accessing notroot@example.com}'
                )
            }, 'tomvn.com': {
                'endpoint': (
                    'tom', 'tomvn.com', 22,
                    '-----BEGIN RSA PRIVATE KEY----- {RSA private key for accessing tom@tomvn.com} -----END RSA PRIVATE KEY-----',
                    'ssh-rsa {RSA public key for accessing tom@tomvn.com}'
                )
            }
        }
    }
    pickle.dump(endpoints, f)
    f.close()

Make sure Twisted is installed and fire up the sshpbx.py script.

Using an SSH key matching the "admin" user in the map\_key\_to\_user file, give it a spin. You'd expect to land on notroot@example.com using the keys configured in map\_request\_to\_endpoint.

    ssh example@localhost -p 2022
