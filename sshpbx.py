#!/usr/bin/env python

from twisted.cred.portal import Portal
from twisted.cred import credentials
from twisted.conch import checkers, error
from twisted.conch.ssh.factory import SSHFactory
from twisted.conch.ssh.keys import Key
from twisted.conch.interfaces import IConchUser
from twisted.conch.avatar import ConchUser
from twisted.internet import reactor, protocol, defer
from zope.interface import implements
from twisted.conch.ssh import connection
from twisted.conch.ssh import userauth

from twisted.python import log, failure
import sys
log.startLogging(sys.stdout)
from twisted.internet.endpoints import TCP4ClientEndpoint

privateKey = Key.fromFile("test_conch_rsa")

publicKey = Key.fromFile("test_conch_rsa.pub")

###### Client start
from twisted.conch.ssh import transport

class ClientSSHTransport(transport.SSHClientTransport):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
    def verifyHostKey(self, pubKey, fingerprint):
        # we need to record this
        return defer.succeed(1)
    def connectionSecure(self):
        self.requestService(ClientUserAuth(self.user.user["endpoint"][0], SSHConnection(send_queue=self.user.client_server_queue, receive_queue=self.user.server_client_queue)))


class ClientUserAuth(userauth.SSHUserAuthClient):
    def getPassword(self, prompt = None):
        # this says we won't do password authentication
        return 
    def getPublicKey(self):
        return Key.fromString(self.transport.user.user["endpoint"][4]).blob()
    def getPrivateKey(self):
        try:
            private_key = Key.fromString(self.transport.user.user["endpoint"][3]).keyObject
        except Exception as e:
            return defer.fail(failure.Failure(error.ConchError("Bad key")))
        return defer.succeed(private_key)

class SSHConnection(connection.SSHConnection):
    def __init__(self, send_queue, receive_queue):
        self.send_queue = send_queue
        self.receive_queue = receive_queue
    def packetReceived(self, *args, **kwargs):
        self.send_queue.put(args)
    def serviceStarted(self):
        self.receive_queue.get().addCallback(self.handle_queue)
    def handle_queue(self, args):
        self.transport.sendPacket(*args)
        self.receive_queue.get().addCallback(self.handle_queue)


##### Client end

class PublicKeyCredentialsChecker:
    implements(checkers.ICredentialsChecker)
    credentialInterfaces = (credentials.ISSHPrivateKey,)

    def raise_error(self, error_msg):
        raise failure.Failure(
            error.ConchError(error_msg))

    @defer.inlineCallbacks
    def requestAvatarId(self, credentials):
        try:
            user = yield map_key_to_user(Key.fromString(data=credentials.blob).toString('OPENSSH'))
        except Exception as e:
            defer.returnValue(failure.Failure(error.ConchError("No such user")))
            #raise failure.Failure(
            #    error.ConchError(e.message))
        try:
            endpoint = yield map_request_to_endpoint(user, credentials.username)
        except Exception as e:
            defer.returnValue(failure.Failure(
                error.ConchError(e.message)))
        defer.returnValue(endpoint)


def nothing():
    pass

class SimpleRealm(object):
    def requestAvatar(self, avatarId, mind, *interfaces):
        user = ConchUser()
        user.user = avatarId
        protocol.ClientCreator(reactor, ClientSSHTransport, user=user).connectTCP(user.user["endpoint"][1], user.user["endpoint"][2])

        user.server_client_queue = defer.DeferredQueue()
        user.client_server_queue = defer.DeferredQueue()


        return IConchUser, user, nothing

@defer.inlineCallbacks
def map_request_to_endpoint(user, endpoint_name):
    import pickle
    with open("map_request_to_endpoint", "rb") as f:
        endpoints = pickle.load(f)
    yield
    try:
        endpoint = endpoints[user][endpoint_name]
    except KeyError:
        raise Exception("{} not found for {}!".format(endpoint, user))
    defer.returnValue(endpoint)

@defer.inlineCallbacks
def map_key_to_user(key):
    import pickle
    with open("map_key_to_user", "rb") as f:
        authorizedKeys = pickle.load(f)
    yield
    try:
        user = authorizedKeys[key]
    except KeyError:
        raise Exception("no user found mathcing key {}!".format(key))
    defer.returnValue(user)

class ServerSSHConnection(connection.SSHConnection):
    def __init__(self, send_queue, receive_queue):
        log.msg("entering init.... - {} and {}".format(send_queue, receive_queue))
    # def __init__(self, send_queue, receive_queue):
        self.send_queue = send_queue
        self.receive_queue = receive_queue
    def serviceStarted(self):
        self.transport.avatar.client_server_queue.get().addCallback(self.handle_queue)
    def packetReceived(self, *args, **kwargs):
        self.transport.avatar.server_client_queue.put(args)
    def handle_queue(self, args):
        self.transport.sendPacket(*args)
        self.transport.avatar.client_server_queue.get().addCallback(self.handle_queue)

class ServerSSHFactory(SSHFactory):
    # services = {
    #     'ssh-userauth': userauth.SSHUserAuthServer,
    #     'ssh-connection': self._start_connection
    # }
    def __init__(self, *args, **kwargs):
        self.services = {
            'ssh-userauth': userauth.SSHUserAuthServer,
            # 'ssh-connection': ServerSSHConnection
            'ssh-connection': self._start_connection
        }
    def _start_connection(self):
        return ServerSSHConnection("test", self)
    def buildProtocol(self, addr):
        testi = SSHFactory.buildProtocol(self, addr)
        log.msg("type(testi) = {}".format(testi))
        return testi
    def setService(self, service):
        log.msg("setting service to {}".format(service))
        SSHFactory.setService(self, service)
    # def getService(self, transport, service):
    #     testi = SSHFactory.getService(self, transport, service)
    #     log.msg("type(testi) = {}".format(testi))
    #     return testi

factory = ServerSSHFactory()
factory.privateKeys = {'ssh-rsa': privateKey}
factory.publicKeys = {'ssh-rsa': publicKey}
factory.portal = Portal(SimpleRealm())
factory.portal.registerChecker(
        PublicKeyCredentialsChecker())

reactor.listenTCP(2022, factory)

reactor.run()
